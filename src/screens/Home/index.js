import React, { Component } from 'react';
import { Carousel } from 'antd';
import * as img from '../../image/index.jpeg';

class Home extends Component {
  render() {
    return (
        <Carousel>
          <div className="panel__img">
            <img src={img} alt="test"/>
          </div>
          <div><h3>2</h3></div>
        </Carousel>
    );
  }
}

export default Home;