import React, { Component } from 'react';
import { Layout, Menu } from 'antd'
import AppRoute from './config/route';
import './styles/main.css';

const { Header, Content, Footer } = Layout;

class App extends Component {
  render() {
    return (
      <Layout style={{height: "100vh"}}>
        <Header className="header">
          <div className="header__logo" />
          <Menu
            className="header__nav"
            mode="horizontal"
            defaultSelectedKeys={['2']}
            style={{ lineHeight: '63px' }}
          >
            <Menu.Item key="1">Trang chủ</Menu.Item>
            <Menu.Item key="2">Giới thiệu</Menu.Item>
            <Menu.Item key="3">Sản phẩm</Menu.Item>
          </Menu>
        </Header>
        <Content className="content">
          <AppRoute />
        </Content>
        <Footer className="footer">
          Công nghệ web tiên tiến Copyright @2018
        </Footer>
      </Layout>
    );
  }
}

export default App;
